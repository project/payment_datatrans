<?php

namespace Drupal\Tests\payment_datatrans\Functional;

use Drupal\Core\Url;
use Drupal\currency\Entity\Currency;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the datatrans payment method.
 *
 * @group payment_datatrans
 */
class DatatransPaymentTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
protected static $modules = array(
    'payment_datatrans',
    'payment',
    'payment_form',
    'payment_datatrans_test',
    'node',
    'field_ui',
    'config'
  );

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to create and edit books and to administer blocks.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Generic node used for testing.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * @var string
   */
  protected $fieldName;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a field name
    $this->fieldName = strtolower($this->randomMachineName());

    // Create article content type
    $node_type = $this->drupalCreateContentType(array(
      'type' => 'article',
      'name' => 'Article'
    ));

    $config_importer = \Drupal::service('currency.config_importer');
    $config_importer->importCurrency('CHF');

    $this->addPaymentFormField($node_type);

    // Create article node
    $title = $this->randomString();

    // Create node with payment plugin configuration
    $this->node = $this->drupalCreateNode(array(
      'type' => 'article',
      $this->fieldName => array(
        'plugin_configuration' => array(
          'amount' => '123',
          'currency_code' => 'CHF',
          'name' => 'payment_basic',
          'payment_id' => NULL,
          'quantity' => '2',
          'description' => 'Payment description',
        ),
        'plugin_id' => 'payment_basic',
      ),
      'title' => $title,
    ));

    // Create user with correct permission.
    $this->adminUser = $this->drupalCreateUser(array(
      'payment.payment_method_configuration.view.any',
      'payment.payment_method_configuration.update.any',
      'access content',
      'access administration pages',
      'access user profiles',
      'payment.payment.view.any'
    ));
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests succesfull Datatrans payment.
   */
  function testDatatransSuccessPayment() {
    // Modifies the datatrans configuration for testing purposes.
    $generator = \Drupal::urlGenerator();
    $datatrans_configuration = array(
      'plugin_form[up_start_url]' => $generator->generateFromRoute('datatrans_test.datatrans_form', array(), array('absolute' => TRUE)),
      'plugin_form[merchant_id]' => '123456789',
      'plugin_form[message][value]' => 'Datatrans',
      'plugin_form[req_type]' => 'CAA',
      'plugin_form[security][security_level]' => '2',
      'plugin_form[security][merchant_control_constant]' => '',
      'plugin_form[security][hmac_key]' => '6543123456789',
      'plugin_form[security][hmac_key_2]' => '',
      'plugin_form[use_alias]' => TRUE,
      'plugin_form[plugin_form][execute_status][container][select][container][plugin_id]' => 'payment_success',
    );
    $this->drupalGet('admin/config/services/payment/method/configuration/payment_datatrans');
    $this->submitForm($datatrans_configuration, t('Save'));

    // Create datatrans payment
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm(array(), t('Pay'));

    // Retrieve plugin configuration of created node
    $plugin_configuration = $this->node->{$this->fieldName}->plugin_configuration;

    // Array of Datatrans payment method configuration.
    $datatrans_payment_method_configuration = \Drupal::entityTypeManager()->getStorage('payment_method_configuration')->load('payment_datatrans')->getPluginConfiguration();

    // Check for correct Merchant ID
    $this->assertSession()->pageTextContains('merchantId' . $datatrans_payment_method_configuration['merchant_id']);

    // Check for correct amount
    $calculated_amount = $this->calculateAmount($plugin_configuration['amount'], $plugin_configuration['quantity'], $plugin_configuration['currency_code']);
    $this->assertSession()->pageTextContains('amount' . $calculated_amount);

    // Check for correct success, error and cancel url
    $this->assertSession()->pageTextContains('successUrl' . $generator->generateFromRoute('payment_datatrans.response_success', array('payment' => 1), array('absolute' => TRUE)));
    $this->assertSession()->pageTextContains('errorUrl' . $generator->generateFromRoute('payment_datatrans.response_error', array('payment' => 1), array('absolute' => TRUE)));
    $this->assertSession()->pageTextContains('cancelUrl' . $generator->generateFromRoute('payment_datatrans.response_cancel', array('payment' => 1), array('absolute' => TRUE)));

    // Check for correct sign with using hmac_key
    $this->assertSession()->pageTextContains('sign' . '309dd30ad0cb07770d3a1ffda64585a9');

    // Finish and save payment
    $this->submitForm(array(), t('Submit'));

    // Check out the payment overview page
    $this->drupalGet('admin/content/payment');

    // Check for correct currency code and payment amount
    $this->assertSession()->pageTextContains('CHF 246.00');

    // Check for correct Payment Method
    $this->assertSession()->pageTextContains('Datatrans');

    // Check payment configuration (city, street & zipcode)
    /** @var \Drupal\payment\Entity\PaymentInterface $payment */
    $payment = \Drupal::entityTypeManager()->getStorage('payment')->load(1);
    $payment_method = $payment->getPaymentMethod();
    if (!$payment_method) {
      throw new \Exception('No payment method');
    }
    $payment_configuration = $payment_method->getConfiguration();
    $this->assertSession()->pageTextNotContains('Failed');
    $this->assertEquals($payment_configuration['uppCustomerCity'], 'city');
    $this->assertEquals($payment_configuration['uppCustomerStreet'], 'street');
    $this->assertEquals($payment_configuration['uppCustomerZipCode'], 'CHE');
    $this->assertEquals($payment_configuration['aliasCC'], '70323122544331174');
    $this->assertEquals($payment_configuration['expm'], '12');
    $this->assertEquals($payment_configuration['expy'], '18');
    $this->assertEquals($payment_configuration['pmethod'], 'VIS');
    $this->assertEquals($payment_configuration['testOnly'], 'yes');
    $this->assertEquals($payment_configuration['authorizationCode'], '1000');
    $this->assertEquals($payment_configuration['responseCode'], '101');
    $this->assertEquals($payment_configuration['uppTransactionId'], \Drupal::state()->get('datatrans.identifier'));

    // Check for detailed payment information
    $this->drupalGet('payment/1');
    $this->assertSession()->pageTextNotContains('Failed');
    $this->assertSession()->pageTextContains('Payment description');
    $this->assertSession()->pageTextContains('CHF 123.00');
    $this->assertSession()->pageTextContains('CHF 246.00');
    $this->assertSession()->pageTextContains('Completed');

    // Change the status that is getting set after successful payments.
    $datatrans_configuration = array(
      'plugin_form[up_start_url]' => Url::fromRoute('datatrans_test.datatrans_form', array(), array('absolute' => TRUE))->toString(),
      'plugin_form[merchant_id]' => '123456789',
      'plugin_form[message][value]' => 'Datatrans',
      'plugin_form[req_type]' => 'CAA',
      'plugin_form[security][security_level]' => '2',
      'plugin_form[security][merchant_control_constant]' => '',
      'plugin_form[security][hmac_key]' => '6543123456789',
      'plugin_form[security][hmac_key_2]' => '',
      'plugin_form[use_alias]' => TRUE,
      'plugin_form[plugin_form][execute_status][container][select][container][plugin_id]' => 'payment_authorized',
    );
    $this->drupalGet('admin/config/services/payment/method/configuration/payment_datatrans');
    $this->submitForm($datatrans_configuration, t('Save'));

    // Create a new payment and verify the status.
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm(array(), t('Pay'));
    $this->submitForm(array(), t('Submit'));
    $this->drupalGet('payment/2');
    $this->assertSession()->pageTextContains('Authorized');
  }

  /**
   * Tests failing Datatrans payment.
   * The test fails by providing an incorrect hmac key.
   */
  function testDatatransFailedPayment() {
    // Modifies the datatrans configuration for testing purposes.
    $datatrans_configuration = array(
      'plugin_form[up_start_url]' => Url::fromRoute('datatrans_test.datatrans_form', array(), array('absolute' => TRUE))->toString(),
      'plugin_form[merchant_id]' => '123456789',
      'plugin_form[message][value]' => 'Datatrans',
      'plugin_form[req_type]' => 'CAA',
      'plugin_form[security][security_level]' => '2',
      'plugin_form[security][merchant_control_constant]' => '',
      'plugin_form[security][hmac_key]' => '1234',
      // For failed test we give a wrong hmac_key
      'plugin_form[security][hmac_key_2]' => '',
    );
    $this->drupalGet('admin/config/services/payment/method/configuration/payment_datatrans');
    $this->submitForm($datatrans_configuration, t('Save'));

    // Create datatrans payment
    \Drupal::state()->set('datatrans.return_url_key', 'error');
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm(array(), t('Pay'));

    // Check for incorrect sign.
    $this->assertSession()->pageTextNotContains('sign309dd30ad0cb07770d3a1ffda64585a9');

    // Finish and save payment
    $this->submitForm(array(), t('Submit'));

    // Check out the payment overview page
    $this->drupalGet('admin/content/payment');
    $this->assertSession()->pageTextContains('Failed');
    $this->assertSession()->pageTextNotContains('Success');

    // Check for detailed payment information
    $this->drupalGet('payment/1');
    $this->assertSession()->pageTextContains('Failed');
    $this->assertSession()->pageTextNotContains('Success');
  }

  /**
   * Tests failing Datatrans payment.
   * The test fails by providing an incorrect hmac key.
   */
  function testDatatransTestSignPayment() {
    // Modifies the datatrans configuration for testing purposes.
    $datatrans_configuration = array(
      'plugin_form[up_start_url]' => Url::fromRoute('datatrans_test.datatrans_form', array(), array('absolute' => TRUE))->toString(),
      'plugin_form[merchant_id]' => '123456789',
      'plugin_form[message][value]' => 'Datatrans',
      'plugin_form[req_type]' => 'CAA',
      'plugin_form[security][security_level]' => '2',
      'plugin_form[security][merchant_control_constant]' => '',
      'plugin_form[security][hmac_key]' => '1234',
      // For failed test we give a wrong hmac_key
      'plugin_form[security][hmac_key_2]' => '',
    );
    $this->drupalGet('admin/config/services/payment/method/configuration/payment_datatrans');
    $this->submitForm($datatrans_configuration, t('Save'));

    // Create datatrans payment
    \Drupal::state()->set('datatrans.return_url_key', 'error');
    \Drupal::state()->set('datatrans.identifier', rand(10, 100));

    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm(array(), t('Pay'));

    // Check for correctly generated sign.
    $generated_sign = $this->generateSign($datatrans_configuration['plugin_form[security][hmac_key]'], $datatrans_configuration['plugin_form[merchant_id]'],
      \Drupal::state()->get('datatrans.identifier'), '24600', 'CHF');
    $this->assertSession()->pageTextContains($generated_sign);

    // Finish and save payment
    $this->submitForm(array(), t('Submit'));

    // Check out the payment overview page
    $this->drupalGet('admin/content/payment');
    $this->assertSession()->pageTextContains('Failed');
    $this->assertSession()->pageTextNotContains('Success');

    // Check for detailed payment information
    $this->drupalGet('payment/1');
    $this->assertSession()->pageTextContains('Failed');
    $this->assertSession()->pageTextNotContains('Success');
  }

  /**
   * Tests cancelled Datatrans payment.
   */
  function testDatatransCancelPayment() {
    // Modifies the datatrans configuration for testing purposes.
    $datatrans_configuration = array(
      'plugin_form[up_start_url]' => Url::fromRoute('datatrans_test.datatrans_form', array(), array('absolute' => TRUE))->toString(),
      'plugin_form[merchant_id]' => '123456789',
      'plugin_form[message][value]' => 'Datatrans',
      'plugin_form[req_type]' => 'CAA',
      'plugin_form[security][security_level]' => '2',
      'plugin_form[security][merchant_control_constant]' => '',
      'plugin_form[security][hmac_key]' => '1234',
      // For failed test we give a wrong hmac_key
      'plugin_form[security][hmac_key_2]' => '',
    );
    $this->drupalGet('admin/config/services/payment/method/configuration/payment_datatrans');
    $this->submitForm($datatrans_configuration, t('Save'));

    // Create datatrans payment
    // form_build_id form_token
    \Drupal::state()->set('datatrans.return_url_key', 'cancel');
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm(array(), t('Pay'));

    $this->submitForm(array(), t('Submit'));

    // Check for incorrect sign.
    $this->assertSession()->pageTextNotContains('sign309dd30ad0cb07770d3a1ffda64585a9');

    // Check out the payment overview page
    $this->drupalGet('admin/content/payment');
    $this->assertSession()->pageTextContains('Cancelled');
    $this->assertSession()->pageTextNotContains('Failed');
    $this->assertSession()->pageTextNotContains('Success');

    // Check for detailed payment information
    $this->drupalGet('payment/1');
    $this->assertSession()->pageTextContains('Cancelled');
    $this->assertSession()->pageTextNotContains('Failed');
    $this->assertSession()->pageTextNotContains('Success');
  }

  /**
   * Calculates the total amount
   *
   * @param $amount
   *  Base amount
   * @param $quantity
   *  Quantity
   * @param $currency_code
   *  Currency code
   * @return int
   *  Returns the total amount
   */
  function calculateAmount($amount, $quantity, $currency_code) {
    $base_amount = $amount * $quantity;
    $currency = Currency::load($currency_code);
    return intval($base_amount * $currency->getSubunits());
  }

  /**
   * Generates the sign
   *
   * @param $hmac_key
   *  hmac key
   * @param $merchant_id
   *  Merchant ID
   * @param $identifier
   * @param $amount
   *  The order amount
   * @param $currency
   *  Currency Code
   * @return string
   *  Returns the sign
   */
  function generateSign($hmac_key, $merchant_id, $identifier, $amount, $currency) {
    $hmac_data = $merchant_id . $amount . $currency . $identifier;
    return hash_hmac('md5', $hmac_data, pack('H*', $hmac_key));
  }

  /**
   * Adds the payment field to the node
   *
   * @param NodeTypeInterface $type
   *   Node type interface type
   *
   * @param string $label
   *   Field label
   *
   * @return \Drupal\Core\Entity\EntityInterface|static
   */
  function addPaymentFormField(NodeTypeInterface $type, $label = 'Payment Label') {
    $field_storage = FieldStorageConfig::create(array(
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => 'payment_form',
    ));
    $field_storage->save();

    $instance = FieldConfig::create(array(
      'field_storage' => $field_storage,
      'bundle' => $type->id(),
      'label' => $label,
      'settings' => array('currency_code' => 'CHF'),
    ));
    $instance->save();

    // Assign display settings for the 'default' and 'teaser' view modes.
    \Drupal::service('entity_display.repository')->getViewDisplay('node', $type->id(), 'default')
      ->setComponent($this->fieldName, array(
        'label' => 'hidden',
        'type' => 'text_default',
      ))
      ->save();

    return $instance;
  }
}

