<?php
/**
 * @file
 * Contains \Drupal\datatrans_test\DatatransForm.
 */

namespace Drupal\payment_datatrans_test;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Entity\Payment;
use Drupal\payment_datatrans\DatatransHelper;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implements an example form.
 */
class DatatransForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'datatrans_form';
  }
  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    foreach ($request->query->all() as $key => $value) {
      $this->messenger()->addStatus($key . $value);
    }

    $payment = Payment::load($request->query->get('refno'));
    $plugin_definition = $payment->getPaymentMethod()->getPluginDefinition();

    $identifier = \Drupal::state()->get('datatrans.identifier') ?: rand(10000, 100000);
    \Drupal::state()->set('datatrans.identifier', $identifier);

    $form_elements = array(
      'security_level' => $request->query->get('security_level'),
      'refno' => $payment->id(),
      'amount' => $request->query->get('amount'),
      'uppTransactionId' => $identifier,
      'uppCustomerFirstName' => 'firstname',
      'uppCustomerCity' => 'city',
      'uppCustomerZipCode' => 'CHE',
      'uppCustomerDetails' => 'yes',
      'uppCustomerStreet' => 'street',
      'currency' => $request->query->get('currency'),
      'status' => 'success',
      'pmethod' => 'VIS',
      'testOnly' => 'yes',
      'authorizationCode' => '1000',
      'responseCode' => '101',
      'datatrans_key' => $request->query->get('datatrans_key'),
    );

    // Add card alias, expire month and year if useAlias was supplied.
    if (!empty($request->query->get('useAlias'))) {
      $form_elements['aliasCC'] = '70323122544331174';
      $form_elements['expm'] = '12';
      $form_elements['expy'] = '18';
    }

    $generated_sign = DatatransHelper::generateSign($plugin_definition['security']['hmac_key'], $request->query->get('merchantId'), $form_elements['uppTransactionId'], $request->query->get('amount'), $request->query->get('currency'));
    $this->messenger()->addStatus($generated_sign);
    $form_elements['sign2'] = \Drupal::state()->get('datatrans.sign') ?: $generated_sign;

    foreach ($form_elements as $key => $value) {
      $form[$key] = array(
        '#type' => 'hidden',
        '#value' => $value,
      );
    }

    // Don't generate the route, use the submitted url.
    $response_url_key = \Drupal::state()->get('datatrans.return_url_key') ?: 'success';
    $response_url = $request->query->get($response_url_key . 'Url');

    $form['#action'] = $response_url;
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus("Validate Form");
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus("Submit Form");
  }
}
