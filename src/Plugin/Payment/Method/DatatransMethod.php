<?php

/**
 * @file
 * Contains \Drupal\payment_datatrans\Plugin\Payment\Method\DatatransMethod.
 */

namespace Drupal\payment_datatrans\Plugin\Payment\Method;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\currency\Entity\Currency;
use Drupal\payment\OperationResult;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodBase;
use Drupal\payment\Response\Response;
use Drupal\payment_datatrans\DatatransHelper;

/**
 * Datatrans payment method.
 *
 * @PaymentMethod(
 *   deriver = "Drupal\payment_datatrans\Plugin\Payment\Method\DatatransDeriver",
 *   id = "payment_datatrans",
 *   label = @Translation("Datatrans")
 * )
 */
class DatatransMethod extends PaymentMethodBase implements ContainerFactoryPluginInterface, ConfigurableInterface {

  /**
   * Stores a configuration.
   *
   * @param string $key
   *   Configuration key.
   * @param mixed $value
   *   Configuration value.
   *
   * @return $this
   */
  public function setConfigField($key, $value) {
    $this->configuration[$key] = $value;

    return $this;
  }

  /**
   * Adds authentication to post requests.
   *
   * @param array $options
   *   Array fed into the post method.
   *
   */
  public function addAuth(&$options) {
    if (!empty($this->pluginDefinition['password'])) {
      $options['auth'] = [
        $this->pluginDefinition['merchant_id'],
        $this->pluginDefinition['password']
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {
    $payment = $this->getPayment();

    // If this payment is not pending, then it was probably already executed.
    // Just return a default operation result.
    // @todo: Improve this.
    if ($payment->getPaymentStatus()->getPluginId() != 'payment_pending') {
      return new OperationResult();
    }

    $generator = \Drupal::urlGenerator();

    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($payment->getCurrencyCode());

    $payment_data = array(
      'merchantId' => $this->pluginDefinition['merchant_id'],
      'amount' => intval($payment->getAmount() * $currency->getSubunits()),
      'currency' => $payment->getCurrencyCode(),
      'refno' => $payment->id(),
      'sign' => NULL,
      'successUrl' => $generator->generateFromRoute('payment_datatrans.response_success', array('payment' => $payment->id()), array('absolute' => TRUE)),
      'errorUrl' => $generator->generateFromRoute('payment_datatrans.response_error', array('payment' => $payment->id()), array('absolute' => TRUE)),
      'cancelUrl' => $generator->generateFromRoute('payment_datatrans.response_cancel', array('payment' => $payment->id()), array('absolute' => TRUE)),
      'security_level' => $this->pluginDefinition['security']['security_level'],
      'datatrans_key' => DatatransHelper::generateDatatransKey($payment),
    );

    // Set the request type.
    if (!empty($this->pluginDefinition['req_type'])) {
      if ($this->pluginDefinition['req_type'] != 'ignore') {
        $payment_data['reqtype'] = $this->pluginDefinition['req_type'];
        // The "conditional" type is a NOA; the difference is that it will
        // trigger extended handling in the success controller.
        if ($this->pluginDefinition['req_type'] == 'conditional') {
          $payment_data['reqtype'] = 'NOA';
        }
      }
    }

    // If security level 2 is configured then generate and use a sign.
    if ($this->pluginDefinition['security']['security_level'] == 2) {
      // Generates the sign.
      $payment_data['sign'] = DatatransHelper::generateSign($this->pluginDefinition['security']['hmac_key'], $this->pluginDefinition['merchant_id'], $payment->id(), $payment_data['amount'], $payment_data['currency']);
    }

    // If use alias option was enabled in method confiuration apply this for
    // this payment method plugin.
    if ($this->pluginDefinition['use_alias']) {
      $payment_data['useAlias'] = 'true';
    }

    $payment->save();

    $response = new Response(Url::fromUri($this->pluginDefinition['up_start_url'], array(
      'absolute' => TRUE,
      'query' => $payment_data,
    )));
    return new OperationResult($response);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function doRefundPayment() {
    $payment = $this->getPayment();
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($payment->getCurrencyCode());
    $configuration = $this->getConfiguration();

    // Build XML request, see
    // https://www.datatrans.ch/showcase/settlement/xml-credit-request.
    $xml = new \SimpleXMLElement('<paymentService/>');
    $xml->addAttribute('version', '1');
    $body = $xml->addChild('body');
    $body->addAttribute('merchantId', $this->pluginDefinition['merchant_id']);
    $transaction = $body->addChild('transaction');
    $transaction->addAttribute('refno', $payment->id());
    $amount = intval($payment->getAmount() * $currency->getSubunits());
    $request = $transaction->addChild('request');
    $request->addChild('amount', $amount);
    $request->addChild('currency', $payment->getCurrencyCode());
    $request->addChild('uppTransactionId', $configuration['uppTransactionId']);
    // XML Credit transaction type.
    $request->addChild('transtype', '06');

    try {
      $xml_string = $xml->saveXML();
      $options = [
        'headers' => [
          'Content-Type' => 'application/xml',
          'Content-Length' => strlen($xml_string),
        ],
        'body' => $xml_string,
      ];
      $this->addAuth($options);
      $response = \Drupal::httpClient()->post($this->pluginDefinition['refund_url'], $options);
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
      \Drupal::logger('payment_datatrans')->error('Request failed for payment ' . $payment->id() . ' to ' . $this->pluginDefinition['refund_url']);
      return;
    }

    try {
      $xml_response = (string) $response->getBody();
      $this->configuration['response'] = $xml_response;
      $response_xml = new \SimpleXMLElement($xml_response);
      // "01 or 02 for a successful transaction"
      if (in_array((string) $response_xml->body->transaction->response->responseCode, ['01', '02'])) {
        $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($this->pluginDefinition['refund_status_id']));
        $this->getPayment()->save();
        return;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
    }

    $this->getPayment()->save();

    // Consider the response invalid if either XML parsing or the responseCode
    // check fails.
    \Drupal::logger('payment_datatrans')->error('Invalid response for payment ' . $payment->id() . ': ' . $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function doRefundPaymentAccess(AccountInterface $account) {
    return $this->getPayment()->getPaymentStatus()->getPluginId() != $this->getRefundStatusId();
  }


  /**
   * Gets the status to set on payment refund.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getRefundStatusId() {
    return $this->pluginDefinition['refund_status_id'];
  }

  /**
   * Cancels the payment.
   */
  public function cancelPayment() {
    $payment = $this->getPayment();
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($payment->getCurrencyCode());
    $configuration = $this->getConfiguration();

    // Build XML request, see
    // https://www.datatrans.ch/showcase/settlement/xml-cancel-request.
    $xml = new \SimpleXMLElement('<paymentService/>');
    $xml->addAttribute('version', '1');
    $body = $xml->addChild('body');
    $body->addAttribute('merchantId', $this->pluginDefinition['merchant_id']);
    $transaction = $body->addChild('transaction');
    $transaction->addAttribute('refno', $payment->id());
    $amount = intval($payment->getAmount() * $currency->getSubunits());
    $request = $transaction->addChild('request');
    $request->addChild('amount', $amount);
    $request->addChild('currency', $payment->getCurrencyCode());
    $request->addChild('uppTransactionId', $configuration['uppTransactionId']);
    // XML Cancel request type.
    $request->addChild('reqtype', 'DOA');

    try {
      $xml_string = $xml->saveXML();
      $options = [
        'headers' => [
          'Content-Type' => 'application/xml',
          'Content-Length' => strlen($xml_string),
        ],
        'body' => $xml_string,
      ];
      $this->addAuth($options);
      $response = \Drupal::httpClient()->post($this->pluginDefinition['refund_url'], $options);
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
      \Drupal::logger('payment_datatrans')->error('Request failed for payment ' . $payment->id() . ' to ' . $this->pluginDefinition['refund_url']);
      return;
    }

    try {
      $xml_response = (string) $response->getBody();
      $this->configuration['response'] = $xml_response;
      $response_xml = new \SimpleXMLElement($xml_response);
      // "01 or 02 for a successful transaction"
      if (in_array((string) $response_xml->body->transaction->response->responseCode, ['01', '02'])) {
        $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($this->pluginDefinition['cancel_status_id']));
        $this->getPayment()->save();
        return;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
    }

    $this->getPayment()->save();

    // Consider the response invalid if either XML parsing or the responseCode
    // check fails.
    \Drupal::logger('payment_datatrans')->error('Invalid response for payment ' . $payment->id() . ': ' . $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function doCapturePayment() {
    $payment = $this->getPayment();
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($payment->getCurrencyCode());
    $configuration = $this->getConfiguration();

    // Build XML request, see
    // https://www.datatrans.ch/showcase/settlement/xml-credit-request.
    $xml = new \SimpleXMLElement('<paymentService/>');
    $xml->addAttribute('version', '1');
    $body = $xml->addChild('body');
    $body->addAttribute('merchantId', $this->pluginDefinition['merchant_id']);
    $transaction = $body->addChild('transaction');
    $transaction->addAttribute('refno', $payment->id());
    $amount = intval($payment->getAmount() * $currency->getSubunits());
    $request = $transaction->addChild('request');
    $request->addChild('amount', $amount);
    $request->addChild('currency', $payment->getCurrencyCode());
    $request->addChild('uppTransactionId', $configuration['uppTransactionId']);
    // For Settlement, omit both reqtype and transtype.

    try {
      $xml_string = $xml->saveXML();
      $options = [
        'headers' => [
          'Content-Type' => 'application/xml',
          'Content-Length' => strlen($xml_string),
        ],
        'body' => $xml_string,
      ];
      $this->addAuth($options);
      $response = \Drupal::httpClient()->post($this->pluginDefinition['refund_url'], $options);
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
      \Drupal::logger('payment_datatrans')->error('Request failed for payment ' . $payment->id() . ' to ' . $this->pluginDefinition['refund_url']);
      return;
    }

    try {
      $xml_response = (string) $response->getBody();
      $this->configuration['response'] = $xml_response;
      $response_xml = new \SimpleXMLElement($xml_response);
      // "01 or 02 for a successful transaction"
      if (in_array((string) $response_xml->body->transaction->response->responseCode, ['01', '02'])) {
        $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($this->pluginDefinition['execute_status_id']));
        $this->getPayment()->save();
        return;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
    }

    $this->getPayment()->save();

    // Consider the response invalid if either XML parsing or the responseCode
    // check fails.
    \Drupal::logger('payment_datatrans')->error('Invalid response for payment ' . $payment->id() . ': ' . $response->getBody());
  }

}
