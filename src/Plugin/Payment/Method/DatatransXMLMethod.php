<?php

/**
 * @file
 * Contains \Drupal\payment_datatrans\Plugin\Payment\Method\DatatransXMLMethod.
 */

namespace Drupal\payment_datatrans\Plugin\Payment\Method;

use Drupal\currency\Entity\Currency;
use Drupal\payment\OperationResult;
use Drupal\payment_datatrans\DatatransHelper;

/**
 * Datatrans XML payment method.
 *
 * The Datatrans XML API is documented at
 * https://pilot.datatrans.biz/showcase/doc/XML_Authorisation.pdf
 *
 * @PaymentMethod(
 *   deriver = "Drupal\payment_datatrans\Plugin\Payment\Method\DatatransXMLDeriver",
 *   id = "payment_datatrans_xml",
 *   label = @Translation("Datatrans XML")
 * )
 */
class DatatransXMLMethod extends DatatransMethod {

  /**
   * The payment status manager.
   *
   * @todo PaymentMethodBase should declare this.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * {@inheritdoc}
   */
  public function doExecutePayment() {
    $payment = $this->getPayment();
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($payment->getCurrencyCode());
    $configuration = $this->getConfiguration();

    $xml = new \SimpleXMLElement('<authorizationService/>');
    $xml->addAttribute('version', '3');
    $body = $xml->addChild('body');
    $body->addAttribute('merchantId', $this->pluginDefinition['merchant_id']);
    $transaction = $body->addChild('transaction');
    $transaction->addAttribute('refno', $payment->id());
    $amount = intval($payment->getAmount() * $currency->getSubunits());
    $request = $transaction->addChild('request');
    $request->addChild('amount', $amount);
    $request->addChild('currency', $payment->getCurrencyCode());
    $request->addChild('aliasCC', $configuration['aliasCC']);
    $request->addChild('expm', $configuration['expm']);
    $request->addChild('expy', $configuration['expy']);

    // Set the request type.
    if (!empty($this->pluginDefinition['req_type']) && $this->pluginDefinition['req_type'] != 'ignore') {
      $request->addChild('reqtype', $this->pluginDefinition['req_type']);
    }

    // If security level 2 is configured then generate and use a sign.
    if ($this->pluginDefinition['security']['security_level'] == 2) {
      // Generates the sign.
      $request->addChild('sign', DatatransHelper::generateSign($this->pluginDefinition['security']['hmac_key'], $this->pluginDefinition['merchant_id'], $payment->id(), $amount, $payment->getCurrencyCode()));
    }

    try {
      $xml_string = $xml->saveXML();
      $options = [
        'headers' => [
          'Content-Type' => 'application/xml',
          'Content-Length' => strlen($xml_string),
        ],
        'body' => $xml_string,
      ];
      $this->addAuth($options);
      $response = \Drupal::httpClient()->post($this->pluginDefinition['authorize_url'], $options);
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
      \Drupal::logger('payment_datatrans')->error('Request failed for payment ' . $payment->id() . ' to ' . $this->pluginDefinition['up_start_url']);
      return;
    }

    try {
      $xml_response = (string) $response->getBody();
      $this->configuration['response'] = $xml_response;
      $response_xml = new \SimpleXMLElement($xml_response);
      // "01 or 02 for a successful transaction"
      if (in_array((string) $response_xml->body->transaction->response->responseCode, ['01', '02'])) {
        $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($this->pluginDefinition['execute_status_id']));
        $this->getPayment()->save();
        return;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('payment_datatrans', $e);
    }

    $this->getPayment()->save();

    // Consider the response invalid if either XML parsing or the responseCode
    // check fails.
    \Drupal::logger('payment_datatrans')->error('Invalid response for payment ' . $payment->id() . ': ' . $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {
    return new OperationResult();
  }

}
