<?php

/**
 * @file
 * Contains \Drupal\payment_datatrans\Plugin\Payment\MethodConfiguration\DatatransConfiguration.
 */

namespace Drupal\payment_datatrans\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBase;
use Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface;
use Drupal\plugin\PluginType\PluginTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the configuration for the Datatrans payment method plugin.
 *
 * @PaymentMethodConfiguration(
 *   description = @Translation("Datatrans XML payment method."),
 *   id = "payment_datatrans_xml",
 *   label = @Translation("Datatrans XML")
 * )
 */
class DatatransXMLConfiguration extends PaymentMethodConfigurationBase implements ContainerFactoryPluginInterface {

  /**
   * The payment status plugin type.
   *
   * @var \Drupal\plugin\PluginType\PluginTypeInterface
   */
  protected $paymentStatusType;

  /**
   * The plugin selector manager.
   *
   * @var \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface
   */
  protected $pluginSelectorManager;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   A string containing the English string to translate.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Interface for classes that manage a set of enabled modules.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, TranslationInterface $string_translation, ModuleHandlerInterface $module_handler, PluginSelectorManagerInterface $plugin_selector_manager, PluginTypeInterface $payment_status_type) {
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition, $string_translation, $module_handler);
    $this->pluginSelectorManager = $plugin_selector_manager;
    $this->paymentStatusType = $payment_status_type;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\plugin\PluginType\PluginTypeManagerInterface $plugin_type_manager */
    $plugin_type_manager = $container->get('plugin.plugin_type_manager');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('module_handler'),
      $container->get('plugin.manager.plugin.plugin_selector'),
      $plugin_type_manager->getPluginType('payment_status')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + array(
      'merchant_id' => '1000011011',
      'password' => '',
      'req_type' => 'CAA',
      'authorize_url' => 'https://payment.datatrans.biz/upp/jsp/XML_authorize.jsp',
      'execute_status_id' => 'payment_success',
      'security' => array(
        'security_level' => 2,
        'merchant_control_constant' => '',
        'hmac_key' => '',
      ),
    );
  }

  /**
   * Sets the Datatrans Merchant ID.
   *
   * @param string $merchant_id
   *   Datatrans Merchant ID
   *
   * @return $this
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setMerchantId($merchant_id) {
    $this->configuration['merchant_id'] = $merchant_id;

    return $this;
  }

  /**
   * Gets the Datatrans Merchant ID.
   *
   * @return string
   *   Unique Merchant Identifier (assigned by Datatrans)
   */
  public function getMerchantId() {
    return $this->configuration['merchant_id'];
  }

  /**
   * Sets the Datatrans Password.
   *
   * @param string $password
   *   Datatrans Password
   *
   * @return $this
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setPassword($password) {
    $this->configuration['password'] = $password;

    return $this;
  }

  /**
   * Gets the Datatrans Password.
   *
   * @return string
   *   Password (set in Datatrans)
   */
  public function getPassword() {
    return $this->configuration['password'];
  }

  /**
   * Sets the Datatrans Authorize Url.
   *
   * @param string $authorize_url
   *   DataTrans Authorize Url
   *
   * @return $this
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setAuthorizeUrl($authorize_url) {
    $this->configuration['authorize_url'] = $authorize_url;

    return $this;
  }

  /**
   * Gets the DataTrans Authorize URL.
   *
   * @return string
   *   Datatrans service URL
   *   UTF-8 encoding: https://payment.datatrans.biz/upp/jsp/upStart.jsp
   *   ISO encoding: https://payment.datatrans.biz/upp/jsp/upStartIso.jsp
   */
  public function getAuthorizeUrl() {
    return $this->configuration['authorize_url'];
  }

  /**
   * Sets the Datatrans Security Level.
   *
   * @param string $security_level
   *   Datatrans Security Level
   *
   * @return $this
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setSecurityLevel($security_level) {
    $this->configuration['security']['security_level'] = $security_level;

    return $this;
  }

  /**
   * Gets the Datatrans Security Level.
   *
   * @return string
   *   The entire data transfer between the merchant's shop application and the
   *   Datatrans payment application is se-cured by the secure SSL protocol.
   *
   *   Security Level 0:
   *   The data transmission is not secured.
   *
   *   Security Level 1:
   *   The data transmission is secured by sending of the parameter sign which
   *   must contain a merchant-specific control value (constant). See Merchant
   *   Control Constant.
   *
   *   Security Level 2:
   *   The data transmission is secured by sending the parameter sign, which
   *   must contain a digital signature generated by a standard HMAC-MD5 hash
   *   procedure and using a merchant-specific encryption key. The HMAC key is
   *   generated by the system and can be changed at any time in the merchant
   *   administration tool https://payment.datatrans.biz.
   */
  public function getSecurityLevel() {
    return $this->configuration['security']['security_level'];
  }

  /**
   * Sets the Datatrans Merchant Control Constant.
   *
   * @param string $merchant_control_constant
   *   Merchant Control Constant
   *
   * @return $this
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setMerchantControlConstant($merchant_control_constant) {
    $this->configuration['security']['merchant_control_constant'] = $merchant_control_constant;

    return $this;
  }

  /**
   * Gets the Datatrans Merchant Control Constant.
   *
   * @return string
   *   This value is generated in the merchant administration tool
   *   https://payment.datatrans.biz. Note that with every change of this value
   *   (which is possible at any time), the interface accepts the current value
   *   only.
   */
  public function getMerchantControlConstant() {
    return $this->configuration['security']['merchant_control_constant'];
  }

  /**
   * Sets the Datatrans HMAC Key.
   *
   * @param string $hmac_key
   *   The HMAC Key.
   *
   * @return $this
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setHmacKey($hmac_key) {
    $this->configuration['security']['hmac_key'] = $hmac_key;

    return $this;
  }

  /**
   * Gets the Datatrans HMAC Key.
   *
   * @return string
   *   The HMAC key is crerated by the system and can be changed at any time in
   *   the merchant administration tool https://payment.datatrans.biz.
   *   - With every change of the key, the interface accepts signature based on
   *     the current key only!
   *   - The key is delivered in hexadecimal format, and it should also be
   *     stored in this format. But before its usage the key must be translated
   *     into byte format!
   *   - “sign2” is only returned in success case.
   */
  public function getHmacKey() {
    return $this->configuration['security']['hmac_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['#element_validate'][] = array($this, 'formElementsValidate');

    $form['merchant_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant-ID'),
      '#default_value' => $this->getMerchantId(),
      '#required' => TRUE,
    );

    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $this->getPassword(),
      '#required' => FALSE,
    );

    $form['authorize_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Authorize URL'),
      '#default_value' => $this->getAuthorizeUrl(),
      '#required' => TRUE,
    );

    $form['req_type'] = array(
      '#type' => 'select',
      '#title' => t('Request Type'),
      '#options' => array(
        'NOA' => t('Authorization only'),
        'CAA' => t('Authorization with immediate settlement'),
        'ignore' => t('According to the setting in the Web Admin Tool'),
      ),
      '#default_value' => $this->getReqType(),
    );

    $url = Url::fromUri('https://pilot.datatrans.biz/showcase/doc/Technical_Implementation_Guide.pdf', ['external' => TRUE])->toString();
    $form['security'] = array(
      '#type' => 'fieldset',
      '#title' => t('Security Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,

      '#description' => t('You should not work with anything else than security level 2 on a productive system. Without the HMAC key there is no way to check whether the data really comes from Datatrans. You can find more details about the security levels in your Datatrans account at UPP ADMINISTRATION -> Security. Or check the technical information in the <a href=":url">Technical_Implementation_Guide</a>', array(':url' => $url)),
    );

    $form['security']['security_level'] = array(
      '#type' => 'select',
      '#title' => t('Security Level'),
      '#options' => array(
        '0' => t('Level 0. No additional security element will be send with payment messages. (not recommended)'),
        '1' => t('Level 1. An additional Merchant-Identification will be send with payment messages'),
        '2' => t('Level 2. Important parameters will be digitally signed (HMAC-MD5) and sent with payment messages'),
      ),
      '#default_value' => $this->getSecurityLevel(),
    );

    $form['security']['merchant_control_constant'] = array(
      '#type' => 'textfield',
      '#title' => t('Merchant control constant'),
      '#default_value' => $this->getMerchantControlConstant(),
      '#description' => t('Used for security level 1'),
      '#states' => array(
        'visible' => array(
          ':input[name="plugin_form[security][security_level]"]' => array('value' => '1'),
        ),
      ),
    );

    $form['security']['hmac_key'] = array(
      '#type' => 'textfield',
      '#title' => t('HMAC Key'),
      '#default_value' => $this->getHmacKey(),
      '#description' => t('Used for security level 2'),
      '#states' => array(
        'visible' => array(
          ':input[name="plugin_form[security][security_level]"]' => array('value' => '2'),
        ),
      ),
    );

    // Enable users to configure the payment status after payment was received.
    $form['plugin_form'] = array(
      '#process' => array(array($this, 'processBuildConfigurationForm')),
      '#type' => 'container',
    );

    return $form;
  }

  /**
   * Implements a form API #process callback.
   *
   * Adds the payment status selector widget.
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {
    $element['execute_status'] = $this->getExecutePaymentStatusSelector($form_state)
      ->buildSelectorForm([], $form_state);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->getExecutePaymentStatusSelector($form_state)
      ->validateSelectorForm(
        $form['plugin_form']['execute_status'],
        $form_state
      );
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->getexecutePaymentStatusSelector($form_state)
      ->submitSelectorForm(
        $form['plugin_form']['execute_status'],
        $form_state
      );
    $this->setExecuteStatusId($this->getExecutePaymentStatusSelector($form_state)->getSelectedPlugin()->getPluginId());
  }

  /**
   * Implements form validate callback for self::formElements().
   */
  public function formElementsValidate(array $element, FormStateInterface $form_state, array $form) {
    $values = NestedArray::getValue($form_state->getValues(), $element['#parents']);

    $this->setMerchantId($values['merchant_id'])
      ->setPassword($values['password'])
      ->setAuthorizeUrl($values['authorize_url'])
      ->setReqType($values['req_type'])
      ->setSecurityLevel($values['security']['security_level'])
      ->setMerchantControlConstant($values['security']['merchant_control_constant'])
      ->setHmacKey($values['security']['hmac_key']);
  }

    /**
     * Gets the payment status selector for the execute phase.
     *
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorInterface
     */
    protected function getExecutePaymentStatusSelector(FormStateInterface $form_state) {
      $plugin_selector = $this->getPaymentStatusSelector($form_state, 'execute', $this->getExecuteStatusId());
      $plugin_selector->setLabel($this->t('Payment execution status'));
      $plugin_selector->setDescription($this->t('The status to set payments to after being executed by this payment method.'));
      return $plugin_selector;
    }

  /**
   * Gets the payment status selector.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param string $type
   * @param string $default_plugin_id
   *
   * @return \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorInterface
   */
  protected function getPaymentStatusSelector(FormStateInterface $form_state, $type, $default_plugin_id) {
    $key = 'payment_status_selector_' . $type;
    if ($form_state->has($key)) {
      $plugin_selector = $form_state->get($key);
    }
    else {
      $plugin_selector = $this->pluginSelectorManager->createInstance('payment_select_list');
      $plugin_selector->setSelectablePluginType($this->paymentStatusType);
      $plugin_selector->setRequired(TRUE);
      $plugin_selector->setCollectPluginConfiguration(FALSE);
      $plugin_selector->setSelectedPlugin($this->paymentStatusType->getPluginManager()->createInstance($default_plugin_id));

      $form_state->set($key, $plugin_selector);
    }

    return $plugin_selector;
  }

  /**
   * Gets the status to set on payment execution.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getExecuteStatusId() {
    return $this->configuration['execute_status_id'];
  }

  /**
   * Sets the status to set on payment execution.
   *
   * @param string $status
   *   The plugin ID of the payment status to set.
   *
   * @return $this
   */
  public function setExecuteStatusId($status) {
    $this->configuration['execute_status_id'] = $status;
    return $this;
  }

  /**
   * Gets the Datatrans Request Type.
   *
   * @return string
   *   The request type specifies whether the transaction has to be immediately
   *   settled or authorized only. There are two request types available:
   *   “NOA” authorization only
   *   “CAA” authorization with immediate settlement in case of successful
   *   authorization; if “reqtype” is not submitted the transaction is
   *   processed according to the setting in the Web Admin Tool (sec-tion “UPP
   *   Administration”).
   */
  public function getReqType() {
    return $this->configuration['req_type'];
  }

  /**
   * Sets the Datatrans Request Type.
   *
   * @param string $req_type
   *   Datatrans Request Type
   *
   * @return \Drupal\payment_datatrans\Plugin\Payment\MethodConfiguration\DatatransConfiguration
   *   The configuration object for the Datatrans payment method plugin.
   */
  public function setReqType($req_type) {
    $this->configuration['req_type'] = $req_type;

    return $this;
  }

}
